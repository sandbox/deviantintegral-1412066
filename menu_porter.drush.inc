<?php

/**
 * @file
 *   Menu Porter drush commands.
 *
 */

/**
 * Implements hook_drush_command().
 */
function menu_porter_drush_command() {
  $items = array();
  $items['menu-porter-export'] = array(
    'description' => "Export Menus to textfiles for porting to other sites.",
    'arguments' => array(
      'menu' => 'Optional argument to export an array that represents the menu into a text file for easy porting to other sites.',
    ),
    'options' => array(
      'dir' => 'Directory of files containing menu exports.',
    ),
    'examples' => array(
      'drush menu-porter-export' => 'Export all menus into individual text files.',
      'drush menu-porter-export primary-links' => 'Export only the primary links menu into the text files.',
      'drush menu-porter-export --dir=/home/me/menus' => 'Export all menus into the specified directory.',
    ),
    'aliases' => array('mpe'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL
  );
  $items['menu-porter-import'] = array(
    'description' => "Import Menus from textfiles for porting to other sites.",
    'arguments' => array(
      'menu' => 'Optional argument to export an array that represents the menu into a text file for easy porting to other sites.',
    ),
    'options' => array(
      'dir' => 'Directory of files containing menu exports.',
    ),
    'examples' => array(
      'drush menu-porter-import' => 'Import all menus from text files that are in the files directory',
      'drush menu-porter-import primary-links' => 'Import only only the primary links menu if the text file contiaining the menu definition exists',
      'drush menu-porter-import --dir=/home/me/menus'
    ),
    'aliases' => array('mpi'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL
  );
  return $items;
}

/**
 * Export drush command callback.
 *
 * @param $menu
 *   Optional parameter of the name of the specific menu to export.
 *
 * @return
 *   TRUE if the export completed sucessfully, or FALSE otherwise.
 */
function drush_menu_porter_export($menu = NULL) {
  if ($dir = drush_get_option('dir')) {
    $filedir = $dir;
  }
  else {
    $filedir = DRUPAL_ROOT . '/menu-export';
  }

  if (file_prepare_directory($filedir, FILE_CREATE_DIRECTORY) == FALSE) {
    drush_set_error('file', "I can't write to the export directory: " . $filedir);
    return FALSE;
  }

  if (!$menu) {
    $menus = array();
    $menus = menu_get_menus();
    foreach ($menus as $menu_name => $value) {
      drush_log("Exporting $menu_name to $menu_name.inc.", 'ok');
      menu_porter_export_write($filedir, $menu_name);
    }
    return TRUE;
  }
  menu_porter_export_write($filedir, $menu);
  return TRUE;
}

/**
 * Import drush command callback.
 *
 * @param $menu
 *   Optional parameter of the name of the specific menu to import.
 *
 * @return
 *   TRUE if the export completed sucessfully, or FALSE otherwise.
 */
function drush_menu_porter_import($menu = '') {
  if ($dir = drush_get_option('dir')) {
    $filedir = $dir;
  }
  else {
    $filedir = DRUPAL_ROOT . '/menu-export';
  }

  if ($menu == '') {
    $menus = array();
    $menus = menu_get_menus();
    foreach ($menus as $menu => $value) {
      // Write every menu for each file we have.
      if (file_exists($filedir . '/' . $menu)) {
        menu_porter_import_write($filedir, $menu);
      }
    }
    return TRUE;
  }
  if (file_exists($filedir . '/' . $menu) == FALSE) {
    drush_set_error('file', 'There\'s not an exported file waiting for me.');
    return FALSE;
  }
  menu_porter_import_write($filedir, $menu);

  return TRUE;
}

